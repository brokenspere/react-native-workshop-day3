import React, { Component } from 'react'
import { View, Text, Button, Alert, StyleSheet } from 'react-native'

class Screen2 extends Component {

    goToScreen1 = () => {
        this.props.history.push('/Screen1')
    }

    UNSAFE_componentWillMount() {
        if (this.props.location && this.props.location.state && this.props.location.state.userName) {
            // Alert.alert('username is', this.props.location.state.userName)
        }
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'green' }}>
                {/* <View style={styles.row1}>
                    <Text style={{ color: 'white' }}>My proifile</Text>
                    <Text>{this.props.location.state.userName}</Text>
                </View>
                <View style={styles.row2}>
                    <Button style={{ width: 50 }} title="GO back to login" onPress={this.goToScreen1} />
                </View> */}

                <View style={styles.row}>
                    <View>
                        <Button 

                            title="back"
                            color="#841584"
                            onPress={this.goToScreen1}
                        />
                    </View>
                    <View style={styles.profileHeaderText}>
                        <Text >My profile</Text>
                    </View>
                </View>

                <View style={{ flex: 1, backgroundColor: 'red' }}>
                    <View>
                        <Text>Username</Text>
                        <Text>{this.props.location.state.userName}</Text>
                    </View>
                </View>


                <View style={[styles.row, styles.profileHeaderText2]}>
                    <Button

                        title="Edit profile"
                        color="#841584"

                    />
                </View>



            </View>
        )
    }
}
const styles = StyleSheet.create({

    backHeaderText: {
        margin: 20,


    },

    profileHeaderText: {
        margin: 14,
        justifyContent: 'flex-start',
        
        flex:1,
       

    },
    profileHeaderText2: {
        margin: 14,
        justifyContent: 'flex-end',
        alignItems: 'center',
        
      

    },


    row: {
        flexDirection: 'row'
    }







})
export default Screen2